package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    public String convert(String c) {
        try {
            double cel = Double.parseDouble(c.replaceAll("[^0-9]", ""));
            return "" + (9.0/5.0 * cel) + 32;
        } catch (Exception ignored) {

        }
        return "not valid";
    }
}
